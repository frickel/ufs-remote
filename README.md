ufs-remote
==========

A HTML Remote for Kathrein UFS Receiver

Replace remote for Kathrein UFS Receiver in a HTML browser over the network.
Only one HTML file, no imports or online connection required.

You can use the [online](https://frickel.gitlab.io/ufs-remote/) version.(optimized, 41,3 KiB)

Tested
--------------------------------------

Tested with FF 3.6, FF10, FF14, IE8 and IE9, Opera, Safari 5.1.7 on MacOSX (and Render Tests with IE6, IE7 and IE10) 
I used a Kathrein UFS-922 Receiver for Tests. 

Instructions
--------------------------------------

* Download index.html
* Add the URL to the UFS Web Frontend, i.e. http://123.33.12.1:9000/
* Control the receiver with the mouse or with the keyboard (to see key code wait with the mouse cursor over the buttons) 

License
--------------------------------------

* [MIT License](http://www.opensource.org/licenses/mit-license.php)
* Contains [JQuery](http://jquery.org/license)

Changelog
--------------------------------------

### Version 1.3.0 (July 28, 2014)

* Update jquery to 1.11.1

### Version 1.2 (March 11, 2013)


* IE Compatible with IE 10
* better CSS for other browser 

### Version 1.1 (Oct. 08, 2012)

* IE Compatible, tested with IE 8 and IE9
    * CSS ajusted
    * conditional comments for IE
    * SVG to PNG 
* JQuery 1.8.2
* URL must not end with '/' 

### Version 1.0

* Initial Version 
